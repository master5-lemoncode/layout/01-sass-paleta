# 00-Plantilla Básica Proyecto

Plantilla que contiene la estructura de archivos básica para la realización de los proyectos

## Instalar dependencies

Ejecutar `npm install` para instalar las dependecias del proyecto.

## Ejecutar servidor de desarrollo

Ejecuta `npm start` para ejecutar el servidor de desarrollo. Navega a `http://localhost:9005/`. Al ejecutar el servidor de desarrollo se abrirá automáticamente el navegador chrome en dicha URL. Revisar el archivo `gulpfile.js` para ajustar el navegador según la plataforma de desarrollo (Windows, Linux u OS x).

## Compilación de SASS

Ejecutar `npm run build` para compilar los archivos css a partir de los archivos de código en SASS si es necesario.
